$(document).ready(function() {
CucumberHTML.timelineItems.pushArray([
  {
    "id": "7f8ca109-7b02-4fe1-9de8-64f5613c56f7",
    "feature": "Login page feature",
    "scenario": "Forgot password link",
    "start": 1695491579573,
    "group": 1,
    "content": "",
    "tags": "",
    "end": 1695491584631,
    "className": "passed"
  },
  {
    "id": "62598610-fadc-4a6f-93ec-f036dd6f0d46",
    "feature": "Login page feature",
    "scenario": "Login page title",
    "start": 1695491574012,
    "group": 1,
    "content": "",
    "tags": "",
    "end": 1695491579568,
    "className": "passed"
  },
  {
    "id": "d61a6762-82cb-4348-9a96-a11313384bde",
    "feature": "My Account page feature",
    "scenario": "MyAccount Information",
    "start": 1695491568234,
    "group": 1,
    "content": "",
    "tags": "",
    "end": 1695491574003,
    "className": "passed"
  },
  {
    "id": "eefa6e60-11b5-4234-b3a5-305257a9eef0",
    "feature": "Login page feature",
    "scenario": "login with correct credentials",
    "start": 1695491584643,
    "group": 1,
    "content": "",
    "tags": "",
    "end": 1695491592425,
    "className": "passed"
  },
  {
    "id": "0d2e4535-b284-4b5a-aa1a-ff2c09e9c975",
    "feature": "My Account page feature",
    "scenario": "MyAccount page title",
    "start": 1695491560782,
    "group": 1,
    "content": "",
    "tags": "",
    "end": 1695491568214,
    "className": "passed"
  }
]);
CucumberHTML.timelineGroups.pushArray([
  {
    "id": 1,
    "content": "Thread[main,5,main]"
  }
]);
});