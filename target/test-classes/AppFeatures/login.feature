Feature: Login page feature

Scenario: Login page title
Given user is on login page
When user gets title of the page
Then the page title should be "Login - My Shop"

Scenario: Forgot password link
Given user is on login page
Then forgot password link should be displayed

Scenario: login with correct credentials
Given user is on login page
When user enters username "irig.prashanth@gmail.com"
And user enters password "209509"
And user clicks on login button
Then user gets title of the page
And the page title should be "My account - My Shop"