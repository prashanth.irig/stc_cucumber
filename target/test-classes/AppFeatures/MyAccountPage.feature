Feature: My Account page feature

Background:
Given user already logged into application with below details:
|username|password|
|irig.prashanth@gmail.com|209509|

Scenario: MyAccount page title
Given user is on Account page
When user gets title of the page
Then the page title should be "My account - My Shop"

Scenario: MyAccount Information
Given user is on Account page
Then user gets account section details
|ADD MY FIRST ADDRESS|
|ORDER HISTORY AND DETAILS|
|MY CREDIT SLIPS|
|MY ADDRESSES|
|MY PERSONAL INFORMATION|
And account details count should be 5
