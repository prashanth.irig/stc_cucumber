package Parallel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

import com.pages.LoginPage;
import com.pages.MyAccountPage;
import com.qa.factory.DriverFactory;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import junit.framework.Assert;

public class MyAccountPageSteps {
	private LoginPage lpage = new LoginPage(DriverFactory.getDriver());
	private MyAccountPage myaccount;
	private String ptitle;
	@Given("user already logged into application with below details:")
	public void user_already_logged_into_application_with_below_details(DataTable dataTable) {
		DriverFactory.getDriver().get("http://www.automationpractice.pl/index.php?controller=authentication&back=my-account");
		List<Map<String,String>> credentials = dataTable.asMaps();
		String uname = credentials.get(0).get("username");
		String pwd = credentials.get(0).get("password");
		//login with details
		myaccount = lpage.doLogin(uname, pwd);
		
		
	}

	@Given("user is on Account page")
	public void user_is_on_account_page() {
		
		ptitle = lpage.getTitleOfLoginPage();
		System.out.println("title :"+ptitle);
	    
	}

	
	@Then("user gets account section details")
	public void user_gets_account_section_details(DataTable dataTable) {
		List<String> sectionList = dataTable.asList();
		System.out.println("Expected list :"+sectionList);
		List<String> actualList = myaccount.getMyAccountPageDetails();
		System.out.println("Actual list :"+actualList);
		Assert.assertTrue(sectionList.containsAll(actualList));
		
	}

	@Then("account details count should be {int}")
	public void account_details_count_should_be(Integer sectionCount) {
	   
		Assert.assertTrue(myaccount.MyAccountSectionCount() == sectionCount);
	}

}
