package Parallel;

import com.pages.LoginPage;
import com.qa.factory.DriverFactory;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class LoginSteps {
	private LoginPage lp = new LoginPage(DriverFactory.getDriver());
	private String pageTitle;

	@Given("user is on login page")
	public void user_is_on_login_page() {
		DriverFactory.getDriver().get("http://www.automationpractice.pl/index.php?controller=authentication&back=my-account");
	}

	@When("user gets title of the page")
	public void user_gets_title_of_the_page() {
		pageTitle = lp.getTitleOfLoginPage();
	}

	@Then("the page title should be {string}")
	public void the_page_title_should_be(String string) {
	    Assert.assertEquals(string, pageTitle);
	}

	@Then("forgot password link should be displayed")
	public void forgot_password_link_should_be_displayed() {
	    Assert.assertTrue(lp.isForgotPasswordLinkExist());
	}

	@When("user enters username {string}")
	public void user_enters_username(String uname) {
	    lp.enterUserName(uname);
	}

	@When("user enters password {string}")
	public void user_enters_password(String password) {
	   lp.enterPassword(password);
	}

	@When("user clicks on login button")
	public void user_clicks_on_login_button() {
	    lp.clickLoginButton();
	}

}
