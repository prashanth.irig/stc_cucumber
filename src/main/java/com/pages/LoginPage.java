package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	private WebDriver driver;
	//by locators
	private By emailId = By.id("email");
	private By pwd = By.id("passwd");
	private By submit = By.id("SubmitLogin");
	private By forgotPassword = By.linkText("Forgot your password?");
	
	//constructor
	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	//page actions
	public String getTitleOfLoginPage()
	{
		return driver.getTitle();
	}
	
	public boolean isForgotPasswordLinkExist()
	{
		return driver.findElement(forgotPassword).isDisplayed();
	}

	public void enterUserName(String uname)
	{
		driver.findElement(emailId).sendKeys(uname);
	}
	
	public void enterPassword(String password)
	{
		driver.findElement(pwd).sendKeys(password);
	}
	
	public void clickLoginButton()
	{
		driver.findElement(submit).click();
	}
	
	public MyAccountPage doLogin(String usname, String pass)
	{
	
		driver.findElement(emailId).sendKeys(usname);
		driver.findElement(pwd).sendKeys(pass);
		driver.findElement(submit).click();
		return new MyAccountPage(driver);
	}
	
}
