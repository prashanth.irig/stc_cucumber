package com.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyAccountPage {
	
	private WebDriver driver;
	
	private By myAccountList = By.cssSelector("div.row.addresses-lists span");
	
	public MyAccountPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	//page actions
	public List<String> getMyAccountPageDetails()
	{
		List<String> acclist = new ArrayList<>();
		List<WebElement> aclist = driver.findElements(myAccountList);
		for(WebElement e:aclist)
		{
			String sectionText = e.getText();
			acclist.add(sectionText);
		}
		return acclist;
		
	}
	
	public int MyAccountSectionCount()
	{
		return driver.findElements(myAccountList).size();
	}

}
